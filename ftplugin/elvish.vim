setl commentstring=#%s
setl isk+=-
setl isi+=-

call ale#linter#Define('elvish', {
                        \   'name': 'elvish-lsp',
						\	'lsp': 'stdio',
                        \   'executable': 'elvish',
                        \   'command': '%e -lsp',
                        \   'output_stream': 'both',
                        \   'project_root': '.',
                        \})
