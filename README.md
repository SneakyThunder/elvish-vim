# Vim plugin for [Elvish](https://github.com/elves/elvish)

---

### Features

* Syntax highlighting
* Basic error checking using elvish lsp (requires [ALE](https://github.com/dense-analysis/ale))
	- Can`t make autocompletions work :(
* Small changes to make it easier to work with the language
