if exists("b:current_syntax")
  finish
endif
let b:current_syntax = 'elvish'


syntax keyword elvishKeywords      fn var set set-env tmp del return nextgroup=elvishType skipwhite
syntax keyword elvishConditional   if elif else
syntax keyword elvishRepeat        while for nextgroup=elvishVar skipwhite
syntax keyword elvishRepeat        each peach
syntax keyword elvishException     try catch finally
syntax keyword elvishUse           use use-mod
syntax keyword elvishErr           deprecate
syntax keyword elvishDelimiter     ":" ";" "|" "{" "}" "(" ")" "&" "~"
syntax keyword elvishOperator      + - = * / % < <= == != > >= <s <=s ==s !=s >s >=s eq not-eq not is and or

syntax keyword elvishBuiltins
		\ "all" "assoc" "base" "bool" "break" "call" "cd" "compact"
		\ "compare" "constantly" "continue" "count" "defer"
		\  "dissoc" "drop" "eawk" "echo" "eval"
		\ "exact-num" "exec" "exit" "external" "fail" "float64"
		\ "from-json" "from-lines" "from-terminated" "-gc"
		\ "get-env" "has-env" "has-external" "has-key" "has-value"
		\ "-ifaddrs" "inexact-num" "keys" "kind-of" "-log"
		\ "make-map" "nop" "ns" "num" "one" "only-bytes"
		\ "only-values" "order" "pprint" "print" "printf"
		\ "put" "rand" "randint" "-randseed" "range" "read-line"
		\ "read-upto" "repeat" "repr" "resolve" "run-parallel"
		\ "search-external" "show" "sleep" "slurp" "src" "-stack"
		\ "styled" "styled-segment" "take" "tilde-abbr" "time" "to-json"
		\ "to-lines" "to-string" "to-terminated" "unset-env" "use-mod" "wcswidth"

syn match   elvishFunction      "\%(^\s*fn\s\+\)\@<=\%(\i\|-\)*"
syn match   elvishFunction		'\v(^\s*\w+|\(\s*\w+|;\s*\w+)'

syntax keyword elvishTodos TODO XXX FIXME NOTE
syntax region   elvishComment   start='#' end="$" contains=elvishTodos


" Constants
syntax match elvishConstBool		'\$\(true\|false\)'
syntax keyword elvishConst			$platform:is-unix $_ $after-chdir $args $before-chdir $buildinfo $nil $notify-bg-jobs $paths $pid $pwd $value-out-indicator $version $unix:rlimits $unix:umask $edit:small-word-abbr $edit:selected-file $edit:rprompt-stale-transformer $edit:prompt-stale-threshold $edit:-prompt-eagerness $edit:prompt $edit:navigation:width-ratio $edit:navigation:binding $edit:max-height $edit:location:workspaces $edit:location:pinned $edit:location:hidden $edit:-instant:binding $edit:history:binding $edit:global-binding $edit:exceptions $edit:-dot $edit:current-command $edit:completion:matcher $edit:completion:binding $edit:completion:arg-completer $edit:command:binding $edit:command-duration $edit:command-abbr $edit:before-readline $edit:after-readline $edit:after-command $edit:add-cmd-filters $edit:abbr

" Numbers
syn match   elvishNumber          "\<\(0[0-7]*\|0[xX]\x\+\|\d\+\)[lL]\=\>"
syn match   elvishNumber          "\(\<\d\+\.\d*\|\.\d\+\)\([eE][-+]\=\d\+\)\=[fFdD]\="
syn match   elvishNumber          "\<\d\+[eE][-+]\=\d\+[fFdD]\=\>"
syn match   elvishNumber          "\<\d\+\([eE][-+]\=\d\+\)\=[fFdD]\>"

" Vars
syn match   elvishVar         	'\$@\?\i\+'
syn match   elvishVar     		'\i\+' contained

syn match   elvishType     		'\i\+' contained
syn match	elvishKey			'&\i\+'

" Strs
syn match   elvishQuoted       '\\.'
syn region  elvishString        matchgroup=elvishStringDelimiter start=+"+ end=+"+ contains=elvishQuoted,@elvishDerefs,@elvishSubst
syn region  elvishString        matchgroup=elvishStringDelimiter start=+'+ end=+'+
syn region  elvishPOSIXString   matchgroup=elvishStringDelimiter start=+\$'+ end=+'+ contains=elvishQuoted

highlight default link elvishTodos	Todo
highlight default link elvishUse	Include
highlight default link elvishErr	Error
highlight default link elvishDelimiter	Delimiter
highlight default link elvishKeywords	Keyword
highlight default link elvishBuiltins	Function
highlight default link elvishRepeat	Repeat
highlight default link elvishException	Exception
highlight default link elvishConditional	Conditional
highlight default link elvishConst	Label
highlight default link elvishConstBool	Boolean
highlight default link elvishOperator	Operator
highlight default link elvishNumber	Number
highlight default link elvishComment	Comment
highlight default link elvishKey		Type
highlight default link elvishType		Type
hi link elvishString            String
hi link elvishStringDelimiter   elvishString
hi link elvishPOSIXString       elvishString
hi link elvishVar             Identifier
hi link elvishFunction          Function
